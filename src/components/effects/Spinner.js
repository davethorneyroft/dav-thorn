import * as React from "react";

function SvgComponent(props) {
  return (
    <svg
      width={150}
      height={150}
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      {...props}
    >
      <symbol id="prefix__a" overflow="visible">
        <circle r={10} />
      </symbol>
      <g transform="translate(75 75)">
        <g id="spinner">
          <use xlinkHref="#prefix__a" x={50} fill="rgba(229, 0, 70,.1)" />
          <use
            xlinkHref="#prefix__a"
            x={50}
            transform="rotate(45)"
            fill="rgba(229, 0, 70,.2)"
          />
          <use
            xlinkHref="#prefix__a"
            x={50}
            transform="rotate(90)"
            fill="rgba(229, 0, 70,.3)"
          />
          <use
            xlinkHref="#prefix__a"
            x={50}
            transform="rotate(135)"
            fill="rgba(229, 0, 70,.4)"
          />
          <use
            xlinkHref="#prefix__a"
            x={50}
            transform="rotate(180)"
            fill="rgba(229, 0, 70,.5)"
          />
          <use
            xlinkHref="#prefix__a"
            x={50}
            transform="rotate(225)"
            fill="rgba(229, 0, 70,.6)"
          />
          <use
            xlinkHref="#prefix__a"
            x={50}
            transform="rotate(270)"
            fill="rgba(229, 0, 70,.7)"
          />
          <use
            xlinkHref="#prefix__a"
            x={50}
            transform="rotate(315)"
            fill="rgba(229, 0, 70,.8)"
          />
        </g>
      </g>
    </svg>
  );
}

export default SvgComponent;
