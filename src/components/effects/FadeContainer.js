import { useLayoutEffect, useRef } from "react";

const FadeContainer = ({ children }) => {
  const fadeContainerRef = useRef();
  useLayoutEffect(() => {
    let currentClassName = fadeContainerRef.current.className;
    if (!currentClassName.includes("fadeOut"))
      currentClassName.replace("fadeOut", " ");

    if (!currentClassName.includes("fadeIn"))
      currentClassName = currentClassName + " fadeIn";

    fadeContainerRef.current.className = currentClassName;

    return () => {
      currentClassName.replace("fadeIn", " fadeOut");
      fadeContainerRef.current.className = currentClassName;
    };
  });
  return (
    <div className="fade-container" ref={fadeContainerRef}>
      {children}
    </div>
  );
};

export default FadeContainer;
