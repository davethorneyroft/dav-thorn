const Header = ({ title, agentName }) => (
  <div className="header">
    <div>
      <span className="header-text">Peoples Energy</span>
    </div>
    <div className="name-display">
      <div>
        <span className="header-text">{"Dave Thorne"}</span>
      </div>
    </div>
  </div>
);

export default Header;
