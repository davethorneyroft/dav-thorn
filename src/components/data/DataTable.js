import Spinner from "../effects/Spinner";
import RTable from "./Table";

const DataTable = ({
  loading = true,
  tabledata = [],
  columns,
  onSelectTableRow,
}) => {
  return (
    <div>
      {loading ? (
        <div className="spinner-container">
          {" "}
          <Spinner className="spinner" />
        </div>
      ) : (
        <div className="table-container">
          {tabledata.length > 0 && (
            <RTable
              columns={columns}
              data={tabledata}
              onSelected={onSelectTableRow}
            />
          )}
        </div>
      )}
    </div>
  );
};

export default DataTable;
