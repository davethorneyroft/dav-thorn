import { useRef } from "react";
import AddMeterReadingMachine from "../../state/addReadingMachine";
import FadeContainer from "../effects/FadeContainer";
import AddButton from "../buttons/AddButton";
import { useMachine } from "@xstate/react";
import MeterReadingInput from "./MeterInput";

const AddMeterReadingComponent = ({
  registerNumber = 5,
  meterId = 101,
  accountId = "100345",
  display = false,
}) => {
  const [state, send] = useMachine(AddMeterReadingMachine);
  const meterReadings = useRef([]);

  function onAddButtonClick() {
    let { current: valueArray } = meterReadings;
    if (valueArray.length === registerNumber && valueArray.every(isNumber)) {
      send({
        type: "NEW_METER_READING",
        payload: {
          accountId,
          meterId,

          date: new Date().now,
        },
      });
    }
  }

  function onInputChange(values) {
    meterReadings.current = values;
  }

  const isNumber = (num) => typeof num === "number";

  return (
    <FadeContainer>
      <div className="container-column">
        {display && (
          <>
            <div className="meter-data">
              <div>
                <span>Meter Number: 1009</span>
              </div>
              <div>
                <span>{`Account Number ${accountId}`}</span>
              </div>
              <div>
                <span>{`Date ${new Date().toISOString().split("T")[0]}`}</span>
              </div>
            </div>
            <div className="meter-read-title"> Enter Meter Reading below:</div>
            <div className="register-number multi-input">
              <MeterReadingInput
                registerNumber={registerNumber}
                onChange={onInputChange}
              />
              <div>
                <AddButton onClick={onAddButtonClick} />
              </div>
            </div>
          </>
        )}
      </div>
    </FadeContainer>
  );
};

export default AddMeterReadingComponent;

/*
 */
