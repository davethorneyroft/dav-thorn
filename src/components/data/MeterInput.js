import { useRef } from "react";

const BACKSPACE = 8;

const MeterReadingInput = ({ registerNumber, onChange }) => {
  const inputtedNumbers = useRef(new Array(registerNumber));
  function handleTab(e) {
    if (e.keyCode !== BACKSPACE) {
      const target = e.target;
      console.log(e.target.value);
      inputtedNumbers.current[e.target.id] = parseInt(e.target.value);
      if (onChange) onChange(inputtedNumbers.current);

      let next = target;
      next = next.nextElementSibling;

      while (next) {
        if (next === null) break;
        if (next.tagName.toLowerCase() === "input") {
          next.focus();
          break;
        }
      }
    }
  }

  const isNumber = (num) => num instanceof Number;

  return (
    <div>
      {Array.from({ length: registerNumber }, (_, index) => (
        <input
          key={index}
          id={index}
          type="text"
          maxLength="1"
          inputMode="numeric"
          pattern="\d+"
          className="register-input"
          onKeyUp={handleTab}
        ></input>
      ))}
    </div>
  );
};

export default MeterReadingInput;
