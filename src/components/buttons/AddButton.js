import AddMeterReadingComponent from "../data/AddMeterReadingComponent";

const AddButton = ({ onClick }) => {
  function onClickBtn(e) {
    if (onClick) onClick(e);
  }
  return (
    <button onClick={onClickBtn} className="minimal-btn">
      Add Reading
    </button>
  );
};

export default AddButton;
