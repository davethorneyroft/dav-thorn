const BreadCrumb = ({ crumbs = [], onClick = () => {} }) => {
  function onItemClick(e) {
    if (crumbs.length > 1 && onClick) {
      onClick(e);
    }
  }
  return (
    <div className="breadcrumb">
      <div>
        <ul>
          {crumbs.map((crumb, index) => (
            <li key={`${index}${crumb}`}>
              <div
                id={crumb}
                className={
                  index === crumbs.length - 1
                    ? "breadcrumb_active"
                    : "breadcrumb_inactive"
                }
                onClick={onItemClick}
              >
                {crumb}
              </div>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default BreadCrumb;
