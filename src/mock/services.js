import { customerMockData, readingMockData, meterMockData } from "./data";
export const getCustomers = () => {
  return new Promise((resolve, reject) => {
    setTimeout(function () {
      resolve(customerMockData);
    }, 800);
  });
};

export const getMeterReadingsById = (id) => {
  return new Promise((resolve, reject) => {
    setTimeout(function () {
      resolve(readingMockData);
    }, 800);
  });
};

export const getMetersByCustomerId = (id) => {
  return new Promise((resolve, reject) => {
    setTimeout(function () {
      resolve(meterMockData);
    }, 800);
  });
};
