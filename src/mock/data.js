const customerTableColumns = [
  {
    Header: "Id",
    accessor: "id",
  },
  {
    Header: "Name",
    accessor: "name",
  },
  {
    Header: "Meters",
    accessor: "meters",
  },
];

const meterTableColumns = [
  {
    Header: "Meter Id",
    accessor: "id",
  },
  {
    Header: "Number of Readings",
    accessor: "number",
  },
  {
    Header: "Last Reading Date",
    accessor: "date",
  },
];

const readingsTableColumns = [
  {
    Header: "Readings (units)",
    accessor: "read",
  },
  {
    Header: "Last Reading Date",
    accessor: "date",
  },
];

const customerMockData = [
  {
    id: "1001",
    name: "Jandy Doe",
    meters: "2",
  },
  {
    id: "1002",
    name: "Zoe Dee",
    meters: "1",
  },
  {
    id: "1003",
    name: "Ahmaed Khan",
    meters: "1",
  },
];
const meterMockData = [
  {
    id: "201",
    number: 3,
    date: "1 January 2021",
  },
  {
    id: "301",
    number: 1,
    date: "1 April 2021",
  },
  {
    id: "401",
    number: 1,
    date: "12 January 2021",
  },
];

const readingMockData = [
  {
    date: "1 January 2021",
    read: "75085",
  },
  {
    date: "1 September 2020",
    read: "45085",
  },
  {
    date: "1 June2020",
    read: "20885",
  },
];

export {
  customerTableColumns,
  customerMockData,
  meterTableColumns,
  readingsTableColumns,
  readingMockData,
  meterMockData,
};
