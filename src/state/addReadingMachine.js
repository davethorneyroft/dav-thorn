import { createMachine, assign, send } from "xstate";
import TableMachine from "./tableMachine";
const AddMeterReadingMachine = createMachine({
  id: "meter reader writer",
  initial: "start",
  context: {
    reading: null,
  },
  states: {
    start: {
      invoke: {
        id: "tableMachine",
        src: TableMachine,
      },
    },
    add: {
      on: {
        READING_ADDED: {
          actions: [
            (ctx, e) => console.log("e.payload", e.payload),
            send(
              { type: "VALID_METER_READING", payload: 1234 },
              {
                to: "tableMachine",
              }
            ),
          ],
        },
      },
      success: {},
      error: {},
    },
  },
});

export default AddMeterReadingMachine;
