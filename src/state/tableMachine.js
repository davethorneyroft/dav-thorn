import { createMachine, assign } from "xstate";
import {
  getCustomers,
  getMeterReadingsById,
  getMetersByCustomerId,
} from "../mock/services";
import {
  customerTableColumns,
  meterTableColumns,
  readingsTableColumns,
} from "../mock/data";

const RETRY_COUNT = 3;
const CUSTOMER_BREADCRUMB = "Customer List";
const METER_BREADCRUMB = "Customer Meter List";
const READ_BREADCRUMB = "Meter Readings List";

function getCustomersByAgent(context, e) {
  if (context.customer_data.length > 0) {
    return context.customer_data;
  } else {
    return getCustomers();
  }
}

function getMeterDataById(context, e) {
  return getMetersByCustomerId(e.id);
}

function getMeterReadingsByMeterId(context, e) {
  return getMeterReadingsById(e.id);
}

const MeterReadsMachine = createMachine({
  id: "meter reader writer",
  initial: "customer_view",
  context: {
    customer_data: [], // cache customers
    meter_data: [],
    data: [], // displayed data
    error: null,
    status: "loading_data",
    retries: 0,
    breadcrumbs: [CUSTOMER_BREADCRUMB],
    currentCustomer: null,
  },
  states: {
    customer_view: {
      initial: "loading",
      states: {
        loading: {
          invoke: {
            id: "fetch-customers",
            src: getCustomersByAgent,
            onDone: {
              target: "loaded",
              actions: assign({
                data: (context, event) => event.data,
                customer_data: (context, event) => event.data,
                columns: customerTableColumns,
                status: "loaded_data",
                retries: (context, e) => context.retries + 1,
              }),
            },
            onError: {
              target: "failed",
              actions: assign({
                error: (context, event) => event.data,
              }),
            },
          },
        },
        loaded: {
          on: {
            REFRESH: "loading",
          },
        },
        failed: {
          on: {
            RETRY: {
              target: "loading",
              cond: (context, e) => context.retries < RETRY_COUNT,
            },
          },
        },
      },
      on: {
        GET_CUSTOMER_METERS: {
          target: "customer_meter_view",
          actions: assign({
            status: "loading_data",
          }),
        },
      },
    },

    customer_meter_view: {
      id: "customer_meter_view",
      initial: "loading",

      states: {
        loading: {
          invoke: {
            id: "fetch-customer_meters",
            src: getMeterDataById,
            actions: assign({
              status: "loading_data",
            }),
            onDone: {
              actions: assign({
                meter_data: (context, event) => event.data,
                data: (context, event) => event.data,
                columns: meterTableColumns,
                status: "loaded_data",
                breadcrumbs: [CUSTOMER_BREADCRUMB, METER_BREADCRUMB],
              }),
            },
            onError: {
              actions: assign({
                error: (context, event) => event.data,
              }),
            },
          },
        },
      },
      on: {
        GET_CUSTOMER_METER_READINGS: {
          target: "customer_meter_reading_view",
          actions: assign({
            status: "loading_data",
          }),
        },
        BEADCRUMB_BACK: {
          target: "customer_view",
          actions: assign({
            data: (context, e) => context.customer_data,
            breadcrumbs: [CUSTOMER_BREADCRUMB],
            columns: customerTableColumns,
          }),
        },
      },
    },
    customer_meter_reading_view: {
      id: "customer_meter_reading_view",
      initial: "loading",

      states: {
        loading: {
          invoke: {
            id: "fetch-customer_meter_readings",
            src: getMeterReadingsByMeterId,
            onDone: {
              actions: assign({
                data: (context, event) => event.data,
                columns: readingsTableColumns,
                status: "loaded_data",
                breadcrumbs: [
                  CUSTOMER_BREADCRUMB,
                  METER_BREADCRUMB,
                  READ_BREADCRUMB,
                ],
              }),
            },
            onError: {
              actions: assign({
                error: (context, event) => event.data,
              }),
            },
          },
        },
      },
      on: {
        BEADCRUMB_BACK: {
          target: "customer_meter_view",
          actions: assign({
            data: (context, e) => context.meter_data,
            breadcrumbs: [CUSTOMER_BREADCRUMB, METER_BREADCRUMB],
            columns: meterTableColumns,
          }),
        },
      },
    },
  },
});

export default MeterReadsMachine;
