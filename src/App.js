import "./App.css";

import MeterReadMachine from "./state/tableMachine";
import { useMachine } from "@xstate/react";
import States from "./state/states";
import BreadCrumb from "./components/naviigation/BreadCrumb";
import Header from "./components/common/Header";
import DataTable from "./components/data/DataTable";
import AddMeterReadingComponent from "./components/data/AddMeterReadingComponent";

const { LOADING } = States;

const App = () => {
  const [state, send] = useMachine(MeterReadMachine);

  function onSelectTableRow(e) {
    if (state.matches("customer_meter_view"))
      send({ type: "GET_CUSTOMER_METER_READINGS", id: e.id });

    if (state.matches("customer_view"))
      send({ type: "GET_CUSTOMER_METERS", id: e.id });
  }

  function onBeadCrumbClick(e) {
    send("BEADCRUMB_BACK");
  }

  const {
    context: { data, columns, value, status, breadcrumbs },
  } = state;
  console.log(state);
  return (
    <div className="container-column">
      <Header />
      <BreadCrumb crumbs={breadcrumbs} onClick={onBeadCrumbClick} />
      <DataTable
        loading={status === LOADING}
        columns={columns}
        tabledata={data}
        staus={status}
        onSelectTableRow={onSelectTableRow}
      />
      <div>
        <AddMeterReadingComponent
          display={
            state.matches("customer_meter_reading_view") && status !== LOADING
          }
        />
      </div>
    </div>
  );
};

export default App;
